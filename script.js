//Damit es auch von Anfang an einen Background und eine Highscore-Anzeige hat
window.onload = function() {
    changeBackground();
    displayHighScores();
}

// Definieren der Variable für das Hindernis
var obstacle = document.getElementById('obstacle');

// Definieren der Variable für das zweite Hindernis
var secondobstacle = document.getElementById('secondobstacle');

// Definieren der Variable für den Spielcharacter
var character = document.getElementById('character');

//Definieren der Variable, um den Hintergrund zu ändern
var background = document.getElementById('game');

//Definieren der Variable für die Radio Buttons, um die Schwierigkeit einzustellen
var radios = document.getElementsByName('difficulty');

//Definieren der Variable Score um den Score während des Spiels zu zählen
var score = 0;
// Standard Punktezähler
var scoreIncrement = 1;
var gameInterval;
var scoreInterval;
// Definieren der Variable für die Game Over Anzeige
var gameOverImage = document.getElementById('game-over');

//Iteriert über die Radio Buttons und fügt jedem Radio Button den Eventlistener 'click' hinzu.
//Beim Klick auf einen Radio Button wird dann die Funktion 'changeBackground' aufgerufen.
radios.forEach(radio => {
    radio.addEventListener('click', changeBackground)
});

//Funktion, um den ausgewählten Radio Button für die Schwierigkeit zu bekommen,
function getCheckedRadio() {
    checkedDifficulty = document.querySelector("input[name='difficulty']:checked").id;
    return checkedDifficulty;
}

//Funktion, um die Radio Buttons zu deaktivieren. Wird in der Funktion 'startGame' aufgerufen, damit man während des Spiels nichts umstellen kann.
function disableRadioButtons() {
    radios.forEach(radio => {
        radio.disabled = true;
    });
}

//Funktion, um die Radio Buttons nach dem Game Over wieder zu aktivieren, damit man die Schwierigkeit umstellen kann.
function enableRadioButtons() {
    radios.forEach(radio => {
        radio.disabled = false;
    });
}

// Funktion, um den Hintergrund zu wechseln je nach ausgewählter Schwierigkeitsstufe.
function changeBackground() {  
    let checkedDifficulty = getCheckedRadio();
    if (checkedDifficulty == 'easy') {
        background.className = 'easy';
    } else if (checkedDifficulty == 'normal') {
        background.className = 'normal';
    } else if (checkedDifficulty == 'hard') {
        background.className = 'hard';
    }
}
// Scorezähler + Score der Schwierigkeitsstufe
function increaseScore() {
    score+= scoreIncrement;
}

function startGame(){
    document.getElementById('start-screen').style.display = 'none';
    disableRadioButtons();
    score = 0;
    //Prüfen ob Spielername eingegeben
    let playerName = document.getElementById('player-name').value;
    if (!playerName) {
        alert('Bitte geben Sie einen Spielernamen ein.');
        enableRadioButtons();
        return;
    }
    document.getElementById('player-name').disabled = true;
    let checkedDifficulty = getCheckedRadio();
    //Prüft, ob der Radio Button 'Einfach' ausgewählt wurde
    if (checkedDifficulty == 'easy') {
        //Fügt die .animation-easy Klasse dem Hindernis-Element hinzu
        obstacle.classList.add('animate-easy');
        // Immer wenn eine Taste gedrückt wird, wird die Funktion 'jumpEasy' ausgeführt
        document.addEventListener("keydown", jumpEasy);
        scoreIncrement = 1;  // 1 Punkt pro Sekunde bei Easy
    //Prüft, ob der Radio Button 'Normal' ausgewählt wurde
    } else if (checkedDifficulty == 'normal') {
        //Fügt die .animation-easy Klasse dem Hindernis-Element hinzu
        obstacle.classList.add('animate-normal');
        // Immer wenn eine Taste gedrückt wird, wird die Funktion 'jumpEasy' ausgeführt
        document.addEventListener("keydown", jumpNormal); 
        scoreIncrement = 2;  // 2 Punkt pro Sekunde bei Normal
    //Prüft, ob der Radio Button'Schwierig' ausgewählt wurde
    } else if (checkedDifficulty == 'hard') {  //In der Schwierigkeitsstufe 'Schwierig' werden die gleichen Animationen hinzugeügt wie bei 'Normal' plus noch ein zusätziches Hindernis.
        //Fügt die .animation-normal Klasse dem Hindernis-Element hinzu
        obstacle.classList.add('animate-normal');
        // Führt die Funktion 'runSecondObstacle' nach 300 Millisekunden aus, welche die Animation für das zweite Hindernis hinzufügt.
        setTimeout(runSecondObstacle, 300);
        // Immer wenn eine Taste gedrückt wird, wird die Funktion 'jumpNormal' ausgeführt
        document.addEventListener('keydown', jumpNormal);
        scoreIncrement = 3;  // 3 Punkt pro Sekunde bei Hard
    }
    // Game Over-Anzeige ausblenden
    gameOverImage.style.display = 'none';
    
    gameInterval = setInterval(checkDead, 10);
    scoreInterval = setInterval(increaseScore, 1000);
}


//Verhindert, dass beim Drücken der Space-Taste der onclick-Event des Buttons aufgeführt wird.
document.addEventListener("keydown", function(event) {
    if (event.key === ' ') { 
      event.preventDefault();
    }
  });

//Fügt die .animation-normal Klasse dem zweiten Hindernis-Element hinzu
function runSecondObstacle() {
    secondobstacle.classList.add('animate-normal');
}


// Fügt im HTML dem Spielcharacter die im CSS definierte Klasse '.animate-jump-easy' hinzu. 
function jumpEasy() {
    if(character.classList == "animate-jump-easy"){return;} // Stoppt, wenn die Animation schon läuft
    character.classList.add('animate-jump-easy');
    setTimeout(removeJumpEasy, 500); // Führt die Funktion 'removeJumpEasy' aus nach 500ms
}

// Fügt im HTML dem Spielcharacter die im CSS definierte Klasse '.animate-jump-normal' hinzu.
function jumpNormal() {
    if(character.classList == "animate-jump-normal"){return;} // Stoppt, wenn die Animation schon läuft
    character.classList.add('animate-jump-normal');
    setTimeout(removeJumpNormal, 300); // Führt die Funktion 'removeJumpNormal' aus nach 300ms
}

//Entfernt die .animate Klasse wieder im HTML für die Schwierigkeitsstufe 'Einfach'
function removeJumpEasy(){
    character.classList.remove('animate-jump-easy');
}

//Entfernt die .animate Klasse wieder im HTML für die Schwierigkeitsstufe 'Normal' und 'Hard'
function removeJumpNormal(){
    character.classList.remove('animate-jump-normal');
}


//Sorgt dafür, dass das Spiel bei einem Game over stoppt und eine Alert-Meldung kommt mit Score und Eintrag des Scores in Highscoreliste
function handleGameOver() {
    clearInterval(gameInterval);
    clearInterval(scoreInterval);
    alert('Game Over. Dein Score: ' + score);
    //Entfert die Animationen für die Hindernisse
    obstacle.classList.remove('animate-easy', 'animate-normal');
    secondobstacle.classList.remove('animate-normal');
    //Entfernt die EventListener für die Sprung-Animation
    document.removeEventListener("keydown", jumpEasy);
    document.removeEventListener("keydown", jumpNormal);
    enableRadioButtons(); // Radio Buttons wieder aktivieren
    document.getElementById('player-name').disabled = false;
    gameOverImage.style.display = 'block';
    saveHighScore();
    // Ausblenden der Game Over Grafik nach 3 Sekunden
    setTimeout(() => {
        gameOverImage.style.display = 'none';
    }, 3000);
    }

//Prüft, ob das Hindernis ungefähr über dem Spielcharacter liegt. Falls ja wird die Funktion handleGameOver ausgeführt.
function checkDead(){
    let characterTop = parseInt(window.getComputedStyle(character).getPropertyValue("top"));
    let obstacleLeft = parseInt(window.getComputedStyle(obstacle).getPropertyValue("left"));
    let secondobstacleLeft = parseInt(window.getComputedStyle(secondobstacle).getPropertyValue('left'));
    if(((obstacleLeft<75  && obstacleLeft>10) || (secondobstacleLeft<75  && secondobstacleLeft>10))     && characterTop>=200){
        handleGameOver();        
    }
} 

// Score in local Storage abspeichern
function saveHighScore() {
    let playerName = document.getElementById('player-name').value;
    let highScores = getHighScores();
    highScores.push({ name: playerName, score: score });
    highScores.sort((a, b) => b.score - a.score);
    highScores = highScores.slice(0, 10); // Nur die Top 10 Scores speichern
    setHighScores(highScores);
    displayHighScores();
}

function getHighScores() {
    let highScores = [];
    let highScoresString = localStorage.getItem('highScores');
    if (highScoresString) {
        highScores = JSON.parse(highScoresString);
    }
    return highScores;
}

// Highscore eintragen
function setHighScores(highScores) {
    localStorage.setItem('highScores', JSON.stringify(highScores));}

// Highscores abrufen  und darstellen
// Tabelle für Highscores darstellen und Rangliste nach Punktezahl definieren
function displayHighScores() {
    let highScores = JSON.parse(localStorage.getItem('highScores')) || [];
    highScores.sort((a, b) => b.score - a.score);

    let highScoresTable = document.querySelector('#high-scores tbody');
    highScoresTable.innerHTML = '';

    highScores.forEach((scoreEntry, index) => {
        let row = document.createElement('tr');
        row.innerHTML = `
            <td>${index + 1}</td>
            <td>${scoreEntry.name}</td>
            <td>${scoreEntry.score}</td>
        `;
        highScoresTable.appendChild(row);
    });
}



